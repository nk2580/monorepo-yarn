const { Command, flags } = require("@oclif/command");
const _fsm = require("fs-magic");
const glob = require("glob")
const path = require("path");
const del = require("node-delete");
const { exec } = require("child-process-async");
const Listr = require("listr");

class SolarschoolsMonorepoYarnCommand extends Command {
  async run() {
    const { flags } = this.parse(SolarschoolsMonorepoYarnCommand);
    const cmd = flags.cmd || "install";
    var currentPath = process.cwd();
    const tasks = new Listr([
      {
        title: "Locate Packages",
        task: async (ctx, task) => {
          return new Listr(
            [
              {
                title: "Finding Function Packages",
                task: async () => {
                  const packages = await this.searchForNodePackages(
                    currentPath,
                    task
                  );
                  ctx.packages = packages;
                }
              },
              {
                title: "Cleaning Yarn Cache",
                task: async () => {
                  task.output = "Cleaning Cache for all Yarn Packages";
                  ctx.packages.forEach(p => {
                    del(p + "/.yarn-cache");
                  });
                }
              }
            ],
            { concurrent: false }
          );
        }
      },
      {
        title: `Run Commands`,
        task: ctx => {
          const setCache = cmd === "install";
          const tasks = ctx.packages.map(pack => {
            const parts = pack.split("/");
            const funcName = parts[parts.length - 1];
            return this.buildPackageTask(pack, cmd, funcName, setCache);
          });
          return new Listr(tasks, { concurrent: 4 });
        }
      }
    ]);
    try {
      await tasks.run();
    } catch (err) {
      this.error(err.message);
    }
  }

  buildPackageTask(p, command, funcName, setCache) {
    return {
      title: `Running ${command} in ${funcName}`,
      task: async () => {
        await this.executeCommand(p, command, setCache, funcName);
      }
    };
  }

  async executeCommand(path, command, setCache, funcName) {
    let stdout, stderr, exitCode;
    try {
      ({ stdout, stderr, exitCode } = await exec(
        `yarn ${command} ${
          setCache
            ? `--cache-folder /tmp/solar-schools/public-api/${funcName}/.yarn-cache`
            : ""
        }`,
        {
          cwd: path
        }
      ));
    } catch (error) {
      throw { message: `${path} \n${error}`, stdout, stderr, exitCode };
    }
  }

  async searchForNodePackages(dir, task) {
    // task.output = "Searching Current Directory...";
    // const everything = await _fsm.scandir(dir, true);
    // task.output = "Filtering node_modules out...";
    // let files = everything[0].filter(f => !f.includes("node_modules"));
    // files = files.filter(f => !f.includes(".yarn-cache"));
    // task.output = "Gathering paths with package.json...";
    // files = files.filter(f => f.includes("package.json"));
    const files = glob.sync("./**/package.json", {ignore: ["./**/node_modules/**", "./**/.yarn-cache/**"]})
    return files.map(f => path.dirname(path.resolve(f)));
  }
}

SolarschoolsMonorepoYarnCommand.description = `Runs Yarn recursively and asynchronously inside this directory`;

SolarschoolsMonorepoYarnCommand.flags = {
  // add --version flag to show CLI version
  version: flags.version({ char: "v" }),
  // add --help flag to show CLI version
  help: flags.help({ char: "h" }),
  cmd: flags.string({ char: "c", description: "command to run" })
};

module.exports = SolarschoolsMonorepoYarnCommand;
